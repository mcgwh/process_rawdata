package main

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"time"

	"github.com/cihub/seelog"
	"github.com/go-ini/ini"
)

type WxConfig struct {
	MysqlConn string
}

var SysConfig WxConfig

func LoadConfig() error {
	cfg, err := ini.Load(CurDir() + "wx.ini")
	if err != nil {
		return err
	}

	SysConfig.MysqlConn = cfg.Section("mysql").Key("MySQLConn").String()

	return nil
}

func InitLog() error {
	logger, err := seelog.LoggerFromConfigAsFile(CurDir() + "log.xml")
	if err != nil {
		fmt.Println(err)
		return err
	}

	seelog.ReplaceLogger(logger)
	return nil
}

func SysInit() error {
	err := InitLog()
	if err != nil {
		return errors.New("InitLog Failed")
	}

	err = LoadConfig()
	if err != nil {
		return errors.New("LoadConfig Failed")
	}

	err = InitOrm()
	if err != nil {
		return errors.New("InitOrm Failed")
	}

	return nil
}

func CurDir() string {
	file, _ := exec.LookPath(os.Args[0])

	path, _ := filepath.Abs(file)
	return filepath.Dir(path) + string(os.PathSeparator)
}

func ComputeSha1(data []byte) []byte {
	hash := sha1.New()

	hash.Write(data)
	return hash.Sum(nil)
}

func ComputeSha1ToHex(data []byte) string {
	return hex.EncodeToString(ComputeSha1(data))
}

func ComputeMd5(data []byte) []byte {
	hash := md5.New()

	hash.Write(data)
	return hash.Sum(nil)
}

func ComputeMd5ToHex(data []byte) string {
	return hex.EncodeToString(ComputeMd5(data))
}

func CurTimestamp() int64 {
	return time.Now().Unix()
}

func UserHashNoAvatar(nick, slogan, country, province, city string, sex int) string {
	sep := "&&"

	data := nick + sep + slogan + sep + country + sep + province + sep + city + sep + strconv.FormatInt(int64(sex), 10)

	return ComputeMd5ToHex([]byte(data))
}
