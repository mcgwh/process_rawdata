package main

import (
	"errors"

	"github.com/cihub/seelog"
)

func UpdateExistUser(old_user *User, u *UserRawResult) error {
	old_user.AvatarSha1 = u.AvatarSha1
	old_user.Nickname = u.Nickname
	old_user.Slogan = u.Slogan
	old_user.Sex = u.Sex
	old_user.Country = u.Country
	old_user.Province = u.Province
	old_user.City = u.City

	if u.Mobile != "" {
		old_user.Mobile = u.Mobile
	}

	if u.WxUsername != "" {
		old_user.WxUsername = u.WxUsername
	}

	if u.WxId != "" {
		old_user.WxId = u.WxId
	}

	UpdateNickHistory(old_user.Id, old_user.Nickname)
	UpdateSloganHistory(old_user.Id, old_user.Slogan)
	UpdateUserStrangerId(old_user.Id, old_user.StrangerId, old_user.StrangerIdMd5)

	if u.Avatar != "" {
		UpdateUserAvatar(old_user.Id, old_user.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)
		UpdateUserHashNoAvatar(old_user.Id, user_hash)

	}

	return UpdateUser(old_user)
}

func CreateNewUserFromRawUser(u *UserRawResult) (error, int) {
	new_user := &User{}

	new_user.AvatarSha1 = u.AvatarSha1
	new_user.City = u.City
	new_user.Country = u.Country
	new_user.CreateTime = u.CreateTime
	new_user.Mobile = u.Mobile
	new_user.Nickname = u.Nickname
	new_user.Province = u.Province
	new_user.Qq = u.Qq
	new_user.Sex = u.Sex
	new_user.Slogan = u.Slogan
	new_user.StrangerId = u.StrangerId
	new_user.StrangerIdMd5 = u.StrangerIdMd5
	new_user.Weibo = u.Weibo
	new_user.WorkerId = u.WorkerId
	new_user.WxId = u.WxId
	new_user.WxUsername = u.WxUsername
	new_user.Source = 1

	err := InsertUser(new_user)
	if err != nil {
		seelog.Errorf("插入新用户失败: %s", err.Error())
		return err, 0
	}

	UpdateNickHistory(new_user.Id, new_user.Nickname)
	UpdateSloganHistory(new_user.Id, new_user.Slogan)
	UpdateUserStrangerId(new_user.Id, new_user.StrangerId, new_user.StrangerIdMd5)

	if u.Avatar != "" {
		UpdateUserAvatar(new_user.Id, new_user.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)
		UpdateUserHashNoAvatar(new_user.Id, user_hash)
	}

	return nil, new_user.Id
}

func ProcessUserItem(u *UserRawResult) (error, int) {
	user := QueryUser(u.WxUsername, u.Mobile, u.WxId)
	if user != nil {
		seelog.Infof("在User表中根据账号信息查到用户，更新之...")

		//update user
		err := UpdateExistUser(user, u)
		//return

		if err != nil {
			seelog.Errorf("更新User失败: %s", err.Error())
			return err, 0
		}
	}

	seelog.Info("未根据账号信息查询到用户，尝试其他方式进行匹配...")

	uid1 := StrangerIdToUid(u.StrangerIdMd5)
	uid2 := -1

	if u.Avatar != "" {
		uid2 = Avatar2UserId(u.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)

		uid2 = UserHash2UserId(user_hash)
	}

	seelog.Infof("ACCOUNT uid1 = %d\tuid2 = %d", uid1, uid2)

	if uid1 < 0 && uid2 < 0 {
		//不存在用户，创建
		seelog.Info("未查询到用户，创建...")

		err, id := CreateNewUserFromRawUser(u)
		if err != nil {
			seelog.Errorf("创建用户失败：%s", err.Error())
			return err, 0
		}

		uid1 = id

		return nil, uid1
	} else if (uid1 == uid2 && uid1 > 0) || (uid1 < 0 && uid2 > 0) || (uid1 > 0 && uid2 < 0) {
		//存在用户，更新
		seelog.Info("查询到用户，更新...")
		if uid1 < uid2 {
			uid1 = uid2
		}

		old_user := UidToUser(uid1)
		if old_user == nil {
			seelog.Errorf("未查询到用户，无法更新...")
			return errors.New("未查询到用户，无法更新..."), 0
		}

		UpdateExistUser(old_user, u)

	} else if uid1 > 0 && uid2 > 0 && uid1 != uid2 {
		//存在用户，并且发现了用户的关联，之前的两个用户其实为同一个用户。。。
		//合并
		seelog.Info("发现了之前未应该合并，但是未识别的用户...")
	}

	return nil, uid1
}

func ProcessUserRawResult() int {
	UpdateUserRawResult()

	count := 0
	for {
		users := GetUserRawResult(30)
		n := len(users)
		if n == 0 {
			seelog.Info("GetUserRawResult: 未获取到数据，break...")
			break
		} else {
			seelog.Infof("GetUserRawResult: 获取到%d条数据", n)
		}

		for _, v := range users {
			seelog.Info("处理账号结果： ", v.Nickname, v.Mobile, v.WxUsername)
			err, id := ProcessUserItem(v)
			if err == nil {
				UpdateUserRawResultStatus(v, id, 2)
			}

		}

		count = count + n
	}

	return count
}
