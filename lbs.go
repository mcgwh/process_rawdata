package main

import (
	"errors"
	"strconv"
	"strings"

	"github.com/cihub/seelog"
)

func Meters(distance string) int {
	sep := ""
	unit := 1

	if strings.Contains(distance, "米") {
		sep = "米"
	} else {
		sep = "公里"
		unit = 1000
	}

	pos := strings.Index(distance, sep)
	if pos > 0 {

		dis := distance[0:pos]
		val, err := strconv.ParseInt(dis, 10, 64)
		if err != nil {
			seelog.Errorf("转换距离失败: %s , %s", distance, err)
			return 0
		}

		return int(val) * unit
	}

	return 0
}

func UpdateExistLbsUser(old_user *User, u *LbsRawResult) error {
	old_user.AvatarSha1 = u.AvatarSha1
	old_user.Nickname = u.Nickname
	old_user.Slogan = u.Slogan
	old_user.Sex = u.Sex
	old_user.Country = u.Country
	old_user.Province = u.Province
	old_user.City = u.City

	UpdateUser(old_user)
	UpdateNickHistory(old_user.Id, old_user.Nickname)
	UpdateSloganHistory(old_user.Id, old_user.Slogan)
	UpdateUserStrangerId(old_user.Id, u.StrangerId, u.StrangerIdMd5)

	if u.Avatar != "" {
		UpdateUserAvatar(old_user.Id, u.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)
		UpdateUserHashNoAvatar(old_user.Id, user_hash)
	}

	InsertLbsResult(u, old_user.Id)

	return UpdateUser(old_user)
}

func CreateNewUserFromLbsRawUser(u *LbsRawResult) error {
	new_user := &User{}

	new_user.AvatarSha1 = u.AvatarSha1
	new_user.City = u.City
	new_user.Country = u.Country
	new_user.CreateTime = u.Createtime
	//new_user.Mobile = u.Mobile
	new_user.Nickname = u.Nickname
	new_user.Province = u.Province
	//	new_user.Qq = u.Qq
	new_user.Sex = u.Sex
	new_user.Slogan = u.Slogan
	new_user.StrangerId = u.StrangerId
	new_user.StrangerIdMd5 = u.StrangerIdMd5
	//	new_user.Weibo = u.Weibo
	new_user.WorkerId = u.WorkerId
	//new_user.WxId = u.WxId
	//new_user.WxUsername = u.WxUsername
	new_user.Source = 0

	err := InsertUser(new_user)
	if err != nil {
		seelog.Errorf("插入新用户失败: %s", err.Error())
		return err
	}

	UpdateNickHistory(new_user.Id, new_user.Nickname)
	UpdateSloganHistory(new_user.Id, new_user.Slogan)

	UpdateUserStrangerId(new_user.Id, new_user.StrangerId, new_user.StrangerIdMd5)

	if u.Avatar != "" {
		UpdateUserAvatar(new_user.Id, new_user.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)
		UpdateUserHashNoAvatar(new_user.Id, user_hash)
	}

	InsertLbsResult(u, new_user.Id)

	return nil
}

func ProcessLbsItem(u *LbsRawResult) error {

	seelog.Info("LBS Start-------------------------------------------------------------")
	defer seelog.Info("LBS End-------------------------------------------------------------")

	seelog.Infof("strangerIdmd5 = %s avatar = %s", u.StrangerIdMd5, u.AvatarSha1)

	uid1 := StrangerIdToUid(u.StrangerIdMd5)
	uid2 := -1

	if u.Avatar != "" {
		uid2 = Avatar2UserId(u.AvatarSha1)
	} else {
		user_hash := UserHashNoAvatar(u.Nickname, u.Slogan, u.Country, u.Province, u.City, u.Sex)

		uid2 = UserHash2UserId(user_hash)
	}

	seelog.Infof("LBS id =%d nickname = %s status = %d\n\tuid1 = %d\tuid2 = %d", u.Id, u.Nickname, u.Status, uid1, uid2)

	if uid1 < 0 && uid2 < 0 {
		//不存在用户，创建
		seelog.Info("未查询到用户，创建...")

		err := CreateNewUserFromLbsRawUser(u)
		if err != nil {
			seelog.Errorf("创建用户失败：%s", err.Error())
			return err
		}

		return nil
	} else if (uid1 == uid2 && uid1 > 0) || (uid1 < 0 && uid2 > 0) || (uid1 > 0 && uid2 < 0) {
		//存在用户，更新
		seelog.Info("查询到用户，更新...")

		if uid1 < uid2 {
			uid1 = uid2
		}

		old_user := UidToUser(uid1)
		if old_user == nil {
			seelog.Errorf("未查询到用户，无法更新...")
			return errors.New("未查询到用户，无法更新...")
		}

		UpdateExistLbsUser(old_user, u)

	} else if uid1 > 0 && uid2 > 0 && uid1 != uid2 {
		//存在用户，并且发现了用户的关联，之前的两个用户其实为同一个用户。。。
		//合并
		seelog.Info("发现了之前未应该合并，但是未识别的用户,暂时先用uid小的那个...")

		if uid1 > uid2 {
			uid1 = uid2
		}

		old_user := UidToUser(uid1)
		if old_user == nil {
			seelog.Errorf("未查询到用户，无法更新...")
			return errors.New("未查询到用户，无法更新...")
		}

		UpdateExistLbsUser(old_user, u)
	}

	return nil
}

func ProcessLbsRawResult() int {
	UpdateLbsRawResult()

	count := 0
	for {
		lbs := GetLbsRawResult(30)
		n := len(lbs)
		if n == 0 {
			seelog.Info("ProcessLbsRawResult: 未获取到数据，break...")
			break
		} else {
			seelog.Infof("UpdateLbsRawResult: 获取到%d条数据", n)
		}

		for _, v := range lbs {
			ProcessLbsItem(v)

			UpdateLbsRawResultStatus(int(v.Id), 2)
		}

		count = count + n
		if count >= 100 {
			seelog.Info("已经处理了100个lbs用户，退出循环...")
			break
		}
	}

	return count
}
