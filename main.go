// processRawData project main.go
package main

import (
	"time"

	"github.com/cihub/seelog"
)

func mainloop() {
	for {
		count := 0
		//1. 合并账号搜索

		n := ProcessUserRawResult()
		seelog.Infof("处理了%d个user_raw_result用户...", n)

		count = count + n

		//2. 合并lbs搜索

		//		time.Sleep(5 * time.Second)

		//		lbss := GetLbsRawResult(10)
		//		for _, v := range lbss {
		//			seelog.Info(*v)
		//		}
		//		count = count + len(lbss)

		n = ProcessLbsRawResult()
		seelog.Infof("处理了%d个lbs_raw_result用户...", n)

		count = count + n

		if count == 0 {
			seelog.Info("未取到需要处理的数据，稍后再试...")
			time.Sleep(20 * time.Second)
		}
	}
}

func main() {
	defer seelog.Flush()

	err := SysInit()
	if err != nil {
		seelog.Errorf("初始化失败: %s", err.Error())
		panic(err)
	}

	/*
		seelog.Info(StrangerIdToUid("113"))
		seelog.Info(Avatar2UserId("7898"))

		UpdateSloganHistory(10, "南国四百八十寺，多少楼台烟雨中...")
		time.Sleep(1 * time.Second)
		UpdateSloganHistory(10, "南国四百八十寺，多少楼台烟雨中...")

		UpdateSloganHistory(10, "南国四百八十寺，多少楼台烟雨中...")

		time.Sleep(1 * time.Second)

		UpdateNickHistory(10, "Jakcy")
		UpdateNickHistory(10, "Jakcy")
		UpdateNickHistory(10, "Vincent")
		UpdateNickHistory(10, "Vincent")
	*/
	//TestUser()
	//seelog.Info(*user)
	mainloop()
}
