package main

import (
	"encoding/base64"
	"net/url"
	"strconv"
)

//心跳信息

/*
{
"i"
"type": 1,##指令类型
"emuid": "emu1", #如果为空串，则对所有模拟器有效
"hostip": "10.0.1.1",
"wxaccout": "130002344",#微信号
”timestamp”: 1468679505 #数据发送的时间
}
*/

type RegisterRequest struct {
	Id        string `json:"id"`
	Type      int    `json:"type"`
	Emuid     string `json:"emuid"`
	Error     int    `json:"error"`
	Timestamp int64  `json:"timestamp"`
	WxAccount string `json:"wxaccout"`
}

type WorkerStatusMessage struct {
	Id        int64   `json:"id"`
	Type      int     `json:"type"`
	Emuid     string  `json:"emuid"`
	Error     int     `json:"error"`
	Timestamp int64   `json:"timestamp"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
}

//LBS Search request
/*
{
"id": 123,
"taskid":2343
"type": 2,##指令类型
"emuid": "emu1",
"longitude":"11.000",
"latitude": "124.3948239"，
”timestamp”: 1468679505 #数据发送的时间
}
*/

type LBSRequest struct {
	Id           int64  `json:"id"`
	TaskId       int    `json:"task_id"`
	Type         int    `json:"type"`
	Emuid        string `json:"emuid"`
	Longitude    string `json:"longitude"`
	Latitude     string `json:"latitude"`
	OrgLongitude string `json:"org_longitude"`
	OrgLatitude  string `json:"org_latitude"`
	Timestamp    int64  `json:"timestamp"`
}

//LBS Search Response
/*
{
"id": 123,
"type": 2,##指令类型
"error": 0,#
"emuid": "emu1",
"task_id":234324,
"longitude":"11.000",
"latitude": "124.3948239"，
”timestamp”: 1468679505 #数据发送的时间
"stranger": [
{ "name": "Brett",
 "city":"Hebei–张家口",
"slogan": "仙人山" ，
"language": "CN" ，
"sex": "男" ，
"avatar": "头像url" ，
“bighead”:”大头像url”,
"distance": "100米" ，###单位也可能是km
"strangerID": "v1_skdfsuskfjsk" ，
“sns_flag”:1 #表示有朋友圈
}
]
}
*/

type Stranger struct {
	Name       string `json:"name"`
	Country    string `json:"country"`
	Province   string `json:"province"`
	City       string `json:"city"`
	Slogan     string `json:"slogan"`
	Language   string `json:"language"`
	Sex        int    `json:"sex"`
	Avatar     string `json:"avatar"`
	Bighead    string `json:"bighead"`
	Distance   string `json:"distance"`
	Strangerid string `json:"strangerID"`
	SnsFlag    int    `json:"sns_flag"`
}

func (s *Stranger) Decode() {
	s.Name, _ = url.QueryUnescape(s.Name)
	s.City, _ = url.QueryUnescape(s.City)
	s.Slogan, _ = url.QueryUnescape(s.Slogan)
	s.Language, _ = url.QueryUnescape(s.Language)
	s.Distance, _ = url.QueryUnescape(s.Distance)
	s.Strangerid, _ = url.QueryUnescape(s.Strangerid)
}

type LBSResponse struct {
	Id           int64      `json:"id"`
	Type         int        `json:"type"`
	Error        int        `json:"error"`
	Emuid        string     `json:"emuid"`
	Timestamp    int64      `json:"timestamp"`
	TaskId       int        `json:"task_id"`
	Longitude    string     `json:"longitude"`
	Latitude     string     `json:"latitude"`
	OrgLongitude string     `json:"org_longitude"`
	OrgLatitude  string     `json:"org_latitude"`
	Strangers    []Stranger `json:"stranger"`
}

func (lbs *LBSResponse) Decode() {
	for idx, _ := range lbs.Strangers {
		lbs.Strangers[idx].Decode()
	}
}

var UserCache map[string]int

//数据库对象Result
//table wx_lbs_result

type LbsRawResult struct {
	Id            int64
	Nickname      string
	Avatar        string
	SmallAvatar   string
	Sex           int
	Slogan        string
	Country       string
	Province      string
	City          string
	Language      string
	StrangerId    string
	Distance      string
	Createtime    int64
	GpsLongitude  float64
	GpsLatitude   float64
	OrgLongitude  string
	OrgLatitude   string
	TaskId        int
	WorkerId      string
	SnsFlag       int
	AvatarSha1    string
	StrangerIdMd5 string
	UrlMd5        string
	Status        int
}

type LbsResult struct {
	Id            int64 `orm:"pk"`
	Uid           int
	Distance      string
	Meters        int
	CreateTime    int64
	Longitude     float64
	Latitude      float64
	TaskLongitude string
	TaskLatitude  string
	TaskId        int
	WorkerId      string
	StrangerId    string
}

//精搜请求
/*
{
"id": 345,
"task_id": 10,
"type": 3,
"emuid": "emu1",
"account":"15871928739",
”timestamp”:1468679505
}
*/

type AccountSearchRequest struct {
	Id        int64  `json:"id"`
	TaskId    int    `json:"task_id"`
	Type      int    `json:"type"`
	Emuid     string `json:"emuid"`
	Account   string `json:"account"`
	Timestamp int64  `json:"timestamp"`
}

//精搜回应
/*

{
"id": 345, #命令id
"task_id": 10,##任务id
"type": 3,##指令类型
"error": 0,
"emuid": "emu1",
”timestamp”:1468679505, #数据发送的时间
"region": "CN_Beijing_Fengtai",##wx新版本的格式
"country": "CN",
"Province": "Shandong",
"city": "Jinan",
"mobile": "1504444", #搜索的内容，电话 or QQ
"nick": "dapeng", ##昵称
"strangerID": "",
"sex": "1",
"alias": "", #不一定取得
"wxid": "wxid6kqa",##不一定都是wxid开头的
"signature": "", ##签名
"smallheadimg":“http://wx.qlogo.cn/mmhead/ver_1/p",
"headimg": "http://wx.qlogo.cn/mmhead/ver_1/ptNoJvFrudfKJ99VjEzzj0p”
“qq”:”365847545”,
“weibo”:”werwer”
}
*/

type User struct {
	Id            int `orm:"auto"`
	WxUsername    string
	Mobile        string
	WxId          string
	Qq            string
	Sex           int
	Weibo         string
	Country       string
	Province      string
	City          string
	Nickname      string
	StrangerId    string
	Avatar        string
	SmallAvatar   string
	Slogan        string
	CreateTime    int64
	WorkerId      string
	AvatarSha1    string
	StrangerIdMd5 string
	Source        int
}

type UserRawResult struct {
	Id            int `orm:"auto"`
	WxUsername    string
	Mobile        string
	WxId          string
	Qq            string
	Sex           int
	Weibo         string
	Country       string
	Province      string
	City          string
	Nickname      string
	StrangerId    string
	Avatar        string
	SmallAvatar   string
	Slogan        string
	CreateTime    int64
	WorkerId      string
	UrlMd5        string
	StrangerIdMd5 string
	Status        int
	AvatarSha1    string
}

type AccountSearchResponse struct {
	Id           int64  `json:"id"`
	TaskId       int    `json:"task_id"`
	Type         int    `json:"type"`
	Error        int    `json:"error"`
	Emuid        string `json:"emuid"`
	Timestamp    int64  `json:"timestamp"`
	Country      string `json:"country"`
	Province     string `json:"Province"`
	City         string `json:"city"`
	Mobile       string `json:"mobile"`
	Nick         string `json:"nick"`
	StrangerID   string `json:"strangerID"`
	Sex          int    `json:"sex"`
	Alias        string `json:"alias"`
	Wxid         string `json:"wxid"`
	Signature    string `json:"signature"`
	Smallheadimg string `json:"smallheadimg"`
	Headimg      string `json:"headimg"`
	Qq           string `json:"qq"`
	Weibo        string `json:"weibo"`
	Account      string `json:"account"`
	UserHash     string `json:"user_hash"`
	ImageData    string `json:"image_data"`
	ImageHash    string `json:"image_hash"`
}

func (resp *AccountSearchResponse) Decode() {
	resp.Nick, _ = url.QueryUnescape(resp.Nick)
	resp.Signature, _ = url.QueryUnescape(resp.Signature)
	resp.StrangerID, _ = url.QueryUnescape(resp.StrangerID)
	resp.Alias, _ = url.QueryUnescape(resp.Alias)
	resp.Wxid, _ = url.QueryUnescape(resp.Wxid)
	resp.Weibo, _ = url.QueryUnescape(resp.Weibo)
}

func (resp *AccountSearchResponse) ComputeUserHash() {
	if resp.ImageHash != "" {
		resp.UserHash = ComputeSha1ToHex([]byte(resp.ImageHash + strconv.FormatInt(int64(resp.Sex), 10)))
	} else {
		if resp.Headimg == "" && resp.Signature != "" {
			resp.UserHash = ComputeSha1ToHex([]byte(resp.Nick + "|" + resp.Signature + "|" + strconv.FormatInt(int64(resp.Sex), 10)))
		}
	}

}

func (resp *AccountSearchResponse) SetImageData(data []byte) {
	if data != nil {
		resp.ImageHash = ComputeSha1ToHex(data)
		resp.ImageData = base64.StdEncoding.EncodeToString(data)
	}

	resp.ComputeUserHash()
}

func (resp *AccountSearchResponse) ToUser() *User {
	user := &User{}

	resp.Decode()

	user.Avatar = resp.Headimg
	user.City = resp.City
	user.Country = resp.Country
	user.CreateTime = resp.Timestamp
	user.Id = 0
	user.Mobile = resp.Mobile
	user.Nickname = resp.Nick
	user.Province = resp.Province
	user.Qq = resp.Qq
	user.Sex = resp.Sex
	user.Slogan = resp.Signature
	user.SmallAvatar = resp.Smallheadimg
	user.StrangerId = resp.StrangerID
	user.WxId = resp.Wxid
	user.WxUsername = resp.Alias
	user.WorkerId = resp.Emuid
	user.Weibo = resp.Weibo

	return user
}

func (resp *AccountSearchResponse) ToUserRawResult() *UserRawResult {
	user := &UserRawResult{}

	resp.Decode()

	user.Avatar = resp.Headimg
	user.City = resp.City
	user.Country = resp.Country
	user.CreateTime = resp.Timestamp
	user.Id = 0
	user.Mobile = resp.Mobile
	user.Nickname = resp.Nick
	user.Province = resp.Province
	user.Qq = resp.Qq
	user.Sex = resp.Sex
	user.Slogan = resp.Signature
	user.SmallAvatar = resp.Smallheadimg
	user.StrangerId = resp.StrangerID
	user.WxId = resp.Wxid
	user.WxUsername = resp.Alias
	user.WorkerId = resp.Emuid
	user.Weibo = resp.Weibo
	user.Status = 0

	return user
}

type AccountSearchTask struct {
	Id         int
	Account    string
	Type       int
	Status     int
	CreateTime int64
	DoneTime   int64
}

type AccountEntry struct {
	Account string
	Type    int
}

func (entry *AccountEntry) ToString() string {
	return entry.Account + strconv.FormatInt(int64(entry.Type), 10)
}

type MomentsRequest struct {
	Id         int64  `json:"id"`
	Uid        int    `json:"uid"`
	TaskId     int    `json:"task_id"`
	Type       int    `json:"type"`
	Emuid      string `json:"emuid"`
	StrangerId string `json:"strangerID"`
	Timestamp  int64  `json:"timestamp"`
}

type MomentsObjects struct {
	Mid          string
	ContentStyle int
	Id           string `json:"Id" orm:"pk"`
	Type         int    `json:"type"`
	ObjDesc      string `json:"Desc"`
	Url          string `json:"Url"`
	Thumb        string `json:"Thumb"`
	Title        string `json:"Title"`
	ThumbType    int    `json:"ThumbType"`
	UrlType      int    `json:"UrlType"`

	Tp         string `json:"tp"`
	Key        string `json:"key" orm:"column(image_key)"`
	Idx        int    `json:"idx"`
	Token      string `json:"token"`
	Ver        int    `json:"ver"`
	CreateTime int64
}

type ContentObjs struct {
	ContentStyle int              `json:"ContentStyle"`
	ContentObjs  []MomentsObjects `json:"ContentObj"`
}

type MomentsDetail struct {
	Id          string      `json:"Id"`
	Uid         int64       `json:"uid"`
	StrangerId  string      `json:"UserName"`
	CreateTime  int64       `json:"CreateTime"`
	ContentDesc string      `json:"ContentDesc"`
	Objs        ContentObjs `json:"ContentObj"`
	CrawlTime   int64
	WorkerId    string
}

type Moments struct {
	Id          string `orm:"pk"`
	StrangerId  string
	Uid         int64
	CreateTime  int64
	ContentDesc string
	CrawlTime   int64
	WorkerId    string
}

func (m *MomentsDetail) toMomentsResult() *Moments {
	res := &Moments{}
	res.Id = m.Id
	res.StrangerId = m.StrangerId
	res.CreateTime = m.CreateTime
	res.ContentDesc = m.ContentDesc
	res.CrawlTime = m.CrawlTime
	res.WorkerId = m.WorkerId
	res.Uid = m.Uid

	return res
}

type MomentsResponse struct {
	Error      int             `json:"error"`
	Id         int64           `json:"id"`
	TaskId     int             `json:"task_id"`
	Uid        int64           `json:"uid"`
	Type       int             `json:"type"`
	Emuid      string          `json:"emuid"`
	StrangerId string          `json:"strangerID"`
	Timestamp  int64           `json:"timestamp"`
	Detail     []MomentsDetail `json:"detail"`
}

func (m *MomentsResponse) Decode() {
	for idx, _ := range m.Detail {
		m.Detail[idx].ContentDesc, _ = url.QueryUnescape(m.Detail[idx].ContentDesc)
		m.Detail[idx].CrawlTime = m.Timestamp
		m.Detail[idx].WorkerId = m.Emuid
		for j, _ := range m.Detail[idx].Objs.ContentObjs {
			m.Detail[idx].Objs.ContentObjs[j].Mid = m.Detail[idx].Id
			m.Detail[idx].Objs.ContentObjs[j].ContentStyle = m.Detail[idx].Objs.ContentStyle
			m.Detail[idx].Objs.ContentObjs[j].ObjDesc, _ = url.QueryUnescape(m.Detail[idx].Objs.ContentObjs[j].ObjDesc)
		}
	}
}

type MomentsTask struct {
	StrangerId    string `orm:"pk"`
	CreateTime    int64
	LastCrawlTime int64
}

//批量搜索
type BatchAccountSearchRequest struct {
	Id          int64    `json:"id"`
	Type        int      `json:"type"`
	Emuid       string   `json:"emuid"`
	TaskId      int      `json:"task_id"`
	AccountList []string `json:"account_list"`
	Timestamp   int64    `json:"timestamp"`
}

type AccountDetail struct {
	Existed      int    `json:"existed"`
	Account      string `json:"account"`
	Country      string `json:"country"`
	Province     string `json:"Province"`
	City         string `json:"city"`
	Mobile       string `json:"mobile"`
	Nick         string `json:"nick"`
	StrangerID   string `json:"strangerID"`
	Sex          int    `json:"sex"`
	Alias        string `json:"alias"`
	Wxid         string `json:"wxid"`
	Signature    string `json:"signature"`
	Smallheadimg string `json:"smallheadimg"`
	Headimg      string `json:"headimg"`
	Qq           string `json:"qq"`
	Weibo        string `json:"weibo"`
}

func (o *AccountDetail) Decode() {
	o.Nick, _ = url.QueryUnescape(o.Nick)
	o.Signature, _ = url.QueryUnescape(o.Signature)
	o.StrangerID, _ = url.QueryUnescape(o.StrangerID)
	o.Alias, _ = url.QueryUnescape(o.Alias)
	o.Wxid, _ = url.QueryUnescape(o.Wxid)
	o.Weibo, _ = url.QueryUnescape(o.Weibo)
}

func (resp *AccountDetail) ToUser() *User {
	user := &User{}

	resp.Decode()

	user.Avatar = resp.Headimg
	user.City = resp.City
	user.Country = resp.Country
	user.CreateTime = CurTimestamp()
	user.Id = 0
	user.Mobile = resp.Mobile
	user.Nickname = resp.Nick
	user.Province = resp.Province
	user.Qq = resp.Qq
	user.Sex = resp.Sex
	user.Slogan = resp.Signature
	user.SmallAvatar = resp.Smallheadimg
	user.StrangerId = resp.StrangerID
	user.WxId = resp.Wxid
	user.WxUsername = resp.Alias
	user.WorkerId = ""

	return user
}

type BatchAccountSearchResponse struct {
	Error     int             `json:"error"`
	Id        int64           `json:"id"`
	TaskId    int             `json:"task_id"`
	Type      int             `json:"type"`
	Emuid     string          `json:"emuid"`
	Timestamp int64           `json:"timestamp"`
	Detail    []AccountDetail `json:"detail"`
}
