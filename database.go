package main

import (
	"github.com/astaxie/beego/orm"
	"github.com/cihub/seelog"
	_ "github.com/go-sql-driver/mysql" // import your used driver
)

type UserNoAvatarHash struct {
	UserHash   string `orm:"pk"`
	Uid        int
	CreateTime int64
}

type UserStranger struct {
	StrangerIdMd5 string `orm:"pk"`
	StrangerId    string
	Uid           int
	CreateTime    int64
}

type UserAvatar struct {
	AvatarSha1 string `orm:"pk"`
	Uid        int
	CreateTime int64
}

func InitOrm() error {
	orm.RegisterModelWithPrefix("wx_", new(LbsRawResult), new(User), new(UserRawResult), new(UserStranger), new(UserAvatar), new(UserNoAvatarHash), new(LbsResult))
	err := orm.RegisterDataBase("default", "mysql", SysConfig.MysqlConn, 2, 5)

	if err != nil {
		seelog.Errorf("初始化ORM失败: %s", err.Error())
	} else {
		seelog.Infof("初始化ORM成功...")
	}

	return err
}

func UpdateUserRawResult() {
	o := orm.NewOrm()

	_, err := o.Raw("UPDATE wx_user_raw_result,wx_avatar set wx_user_raw_result.status = 1, wx_user_raw_result.avatar_sha1=wx_avatar.avatar_sha1 where wx_user_raw_result.url_md5=wx_avatar.url_md5 and wx_avatar.status=1 and wx_user_raw_result.status<2···").Exec()
	if err != nil {
		seelog.Errorf("UpdateUserRawResult失败: %s", err.Error())
	}
}

func UpdateLbsRawResult() {
	o := orm.NewOrm()

	_, err := o.Raw("UPDATE wx_lbs_raw_result,wx_avatar set wx_lbs_raw_result.status = 1, wx_lbs_raw_result.avatar_sha1=wx_avatar.avatar_sha1 where wx_lbs_raw_result.url_md5=wx_avatar.url_md5 and wx_avatar.status=1  and wx_lbs_raw_result.status<2").Exec()
	if err != nil {
		seelog.Errorf("UpdateLbsRawResult失败: %s", err.Error())
	}
}

func GetUserRawResult(max int) []*UserRawResult {
	o := orm.NewOrm()

	//去除“禁止访问图片”对数据的干扰
	o.Raw("update  wx_user_raw_result set avatar = '', avatar_sha1 = '', status = 0 where status <2 and url_md5 in (SELECT url_md5 from wx_avatar where avatar_sha1 = 'eef4f463f08c5d1bfc004533ff9af936041ee362')").Exec()

	var users []*UserRawResult

	n, err := o.QueryTable("wx_user_raw_result").Filter("status", 1).OrderBy("id").Limit(max).All(&users)
	if err != nil {
		seelog.Errorf("GetUserRawResult 失败: %s", err.Error())
	}

	seelog.Infof("GetUserRawResult 获取到了%d个有头像的用户需要处理...", n)

	if n > 0 {
		return users
	}

	n, err = o.QueryTable("wx_user_raw_result").Filter("status", 0).Filter("avatar", "").OrderBy("id").Limit(max).All(&users)
	if err != nil {
		seelog.Errorf("GetUserRawResult 失败: %s", err.Error())
	}

	seelog.Infof("GetUserRawResult 获取到了%d个没有头像的用户需要处理...", n)

	if n > 0 {
		return users
	}

	return nil
}

func GetLbsRawResult(max int) []*LbsRawResult {
	o := orm.NewOrm()

	//去除“禁止访问图片”对数据的干扰
	o.Raw("update  wx_lbs_raw_result set avatar = '', avatar_sha1 = '', status = 0 where status <2 and url_md5 in (SELECT url_md5 from wx_avatar where avatar_sha1 = 'eef4f463f08c5d1bfc004533ff9af936041ee362')").Exec()

	var users []*LbsRawResult

	n, err := o.QueryTable("wx_lbs_raw_result").Filter("status", 1).OrderBy("id").Limit(max).All(&users)
	if err != nil {
		seelog.Errorf("GetUserRawResult 失败: %s", err.Error())
	}

	seelog.Infof("GetLbsRawResult: 获取到了%d个有头像用户需要处理...", n)

	if n > 0 {
		return users
	}

	n, err = o.QueryTable("wx_lbs_raw_result").Filter("status", 0).Filter("avatar", "").OrderBy("id").Limit(max).All(&users)
	if err != nil {
		seelog.Errorf("GetUserRawResult 失败: %s", err.Error())
	}

	seelog.Infof("GetLbsRawResult: 获取到了%d个没有头像用户需要处理...", n)

	return users
}

func QueryUser(wxusername, mobile, wxid string) *User {
	o := orm.NewOrm()
	cond := orm.NewCondition()

	n := 0

	if wxusername != "" {
		cond = cond.Or("wx_username", wxusername)
		n++
	}

	if mobile != "" {
		cond = cond.Or("mobile", mobile)
		n++
	}

	if wxid != "" {
		cond = cond.Or("wx_id", wxid)
		n++
	}

	if n == 0 {
		return nil
	}

	//cond1 := cond.Or("wx_username", wxusername).Or("mobile", mobile).Or("wx_id", wxid)

	var user User

	err := o.QueryTable("wx_user").SetCond(cond).One(&user)
	if err != nil {
		seelog.Error("查询失败：", err)
		return nil
	}

	//seelog.Debug(user)

	return &user
}

func TestUser() *User {
	o := orm.NewOrm()
	cond := orm.NewCondition()

	cond1 := cond.Or("wx_username", "jky527449625").Or("mobile", "18301371081").Or("wx_id", "wxid1bsx99ta00tk21")

	var user User

	err := o.QueryTable("wx_user").SetCond(cond1).One(&user)
	if err != nil {
		seelog.Error("查询失败：", err)
		return nil
	}

	seelog.Info(user)
	//	seelog.Infof("查到了%d个数据", n)

	//	for _, v := range user {
	//		seelog.Info(*v)
	//	}

	return nil
}

func StrangerIdToUid(strangerMd5 string) int {
	o := orm.NewOrm()

	seelog.Infof("StrangerIdMd5 = %s", strangerMd5)

	var user UserStranger
	err := o.QueryTable("wx_user_stranger").Filter("stranger_id_md5", strangerMd5).One(&user)
	if err != nil {
		seelog.Errorf("@@未查询到strangerIdMd5(%s)对应的uid... ", strangerMd5)
		return -1
	}

	return user.Uid
}

func Avatar2UserId(avatarSha1 string) int {
	o := orm.NewOrm()

	seelog.Infof("avatarSha1 = %s", avatarSha1)

	var user UserAvatar
	err := o.QueryTable("wx_user_avatar").Filter("avatar_sha1", avatarSha1).One(&user)
	if err != nil {
		seelog.Errorf("avatar_sha1(%s)对应的uid... ", avatarSha1)

		return -1
	}

	return user.Uid
}

func UserHash2UserId(userhash string) int {
	o := orm.NewOrm()

	var user UserNoAvatarHash
	err := o.QueryTable("wx_user_no_avatar_hash").Filter("user_hash", userhash).One(&user)
	if err != nil {
		seelog.Errorf("未查询到UserHash2UserId(%s)对应的uid: %s ", userhash, err.Error())

		return -1
	}

	return user.Uid
}

func UidToUser(uid int) *User {
	o := orm.NewOrm()

	var user User
	err := o.QueryTable("wx_user").Filter("id", uid).One(&user)
	if err != nil {
		seelog.Errorf("uid(%d)对应的user... ", uid)

		return nil
	}

	return &user
}

func InsertUser(user *User) error {
	o := orm.NewOrm()

	id, err := o.Insert(user)
	if err != nil {
		seelog.Errorf("插入User失败： %s", err.Error())
		return err
	}

	seelog.Errorf("插入User成功，id = %d", id)
	user.Id = int(id)

	return nil
}

func UpdateUser(user *User) error {
	o := orm.NewOrm()

	_, err := o.Update(user)
	if err != nil {
		seelog.Errorf("插入User失败： %s", err.Error())
		return err
	}

	return nil
}

//SELECT uid, content from wx_slogan_history where id = (select max(id) from wx_slogan_history where uid=1)

func UpdateNickHistory(uid int, content string) error {
	sql := `insert into wx_nickname_history  (uid, nickname, create_time) 
          SELECT ?, ?,? from DUAL  where not EXISTS 
          (SELECT nickname from wx_nickname_history where id = (select max(id) from wx_nickname_history where uid=?) and nickname =?)`

	o := orm.NewOrm()

	_, err := o.Raw(sql, uid, content, CurTimestamp(), uid, content).Exec()
	if err != nil {
		seelog.Errorf("UpdateNickHistory 失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateSloganHistory(uid int, content string) error {
	if content == "" {
		return nil
	}

	sql := `insert into wx_slogan_history  (uid, content, create_time) 
          SELECT ?, ?,? from DUAL  where not EXISTS 
          (SELECT content from wx_slogan_history where id = (select max(id) from wx_slogan_history where uid=?) and content =?)`

	o := orm.NewOrm()

	_, err := o.Raw(sql, uid, content, CurTimestamp(), uid, content).Exec()
	if err != nil {
		seelog.Errorf("UpdateSloganHistory 失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateUserAvatar(uid int, avatarSha1 string) error {
	sql := "insert into wx_user_avatar(avatar_sha1,uid,create_time) values(?, ?, ?)"

	o := orm.NewOrm()
	_, err := o.Raw(sql, avatarSha1, uid, CurTimestamp()).Exec()
	if err != nil {
		seelog.Errorf("UpdateUserAvatar 失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateUserHashNoAvatar(uid int, userhash string) error {
	sql := "insert into wx_user_no_avatar_hash(user_hash,uid,create_time) values(?, ?, ?)"

	o := orm.NewOrm()
	_, err := o.Raw(sql, userhash, uid, CurTimestamp()).Exec()
	if err != nil {
		seelog.Errorf("UpdateUserHashNoAvatar 失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateUserStrangerId(uid int, strangerId string, strangerIdMd5 string) error {
	seelog.Infof("UpdateUserStrangerId: strangerIdMd5 = %s uid = %d strangerId=%s", strangerIdMd5, uid, strangerId)

	sql := "insert into wx_user_stranger (stranger_id_md5, stranger_id, uid, create_time) values(?, ?, ?, ?)"

	o := orm.NewOrm()
	_, err := o.Raw(sql, strangerIdMd5, strangerId, uid, CurTimestamp()).Exec()
	if err != nil {
		seelog.Errorf("UpdateUserStrangerId 失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateUserRawResultStatus(u *UserRawResult, uid, status int) error {
	sql := "update wx_user_raw_result set status = ? where id = ?"

	o := orm.NewOrm()
	_, err := o.Raw(sql, status, u.Id).Exec()
	if err != nil {
		seelog.Errorf("UpdateUserRawResultStatus 失败： %s", err.Error())
		return err
	}

	sql2 := "UPDATE wx_account_task set wx_account_task.uid = ?,flg=1 where uid = ?"
	_, err = o.Raw(sql2, uid, u.Id).Exec()
	if err != nil {
		seelog.Errorf("UpdateUserRawResultStatus,更新wx_account_task失败： %s", err.Error())
		return err
	}

	return nil
}

func UpdateLbsRawResultStatus(id, status int) error {
	sql := "update wx_lbs_raw_result set status = ? where id = ?"

	o := orm.NewOrm()
	_, err := o.Raw(sql, status, id).Exec()
	if err != nil {
		seelog.Errorf("UpdateLbsRawResultStatus 失败： %s", err.Error())
		return err
	}

	return nil
}

func InsertLbsResult(u *LbsRawResult, uid int) error {
	o := orm.NewOrm()

	lbs := &LbsResult{}

	lbs.Uid = uid
	lbs.CreateTime = u.Createtime
	lbs.Distance = u.Distance
	lbs.Latitude = u.GpsLatitude
	lbs.Longitude = u.GpsLongitude
	lbs.StrangerId = u.StrangerId
	lbs.TaskId = u.TaskId
	lbs.TaskLatitude = u.OrgLatitude
	lbs.TaskLongitude = u.OrgLongitude
	lbs.WorkerId = u.WorkerId

	lbs.Meters = Meters(lbs.Distance)

	_, err := o.Insert(lbs)
	if err != nil {
		seelog.Errorf("插入lbs_result失败: %s", err.Error())
		return err
	}

	seelog.Info("插入LBS记录： ", lbs)

	return nil
}
